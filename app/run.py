import logging.config
import os

from fastapi import FastAPI

from fastapi.exceptions import RequestValidationError
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.gzip import GZipMiddleware
from fastapi.middleware.cors import CORSMiddleware

from app.config import (
    PROJECT_NAME,
    API_PREFIX,
    WEB_PREFIX,
)


def create_app(name: str = PROJECT_NAME) -> FastAPI:
    application = FastAPI(title=name, openapi_url=f"{API_PREFIX}/openapi.json")
    f_app.add_middleware(JsonAPIErrorsMiddleware)
