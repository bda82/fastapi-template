import typing as t


class AppLogicException(Exception):

    def __init__(
        self,
        msg: str,
        http_status_code: t.Optional[int] = None,
        status_code: t.Optional[int] = None,
        *args,
    ):
        super().__init__(msg, http_status_code, status_code, *args)
        self.msg = msg
        self.http_status_code = http_status_code
        self.error_code = status_code
