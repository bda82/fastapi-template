import logging
from fastapi import status, FastAPI
from fastapi.requests import Request
from starlette.middleware.base import BaseHTTPMiddleware, RequestResponseEndpoint
from fastapi.responses import JSONResponse, Response, PlainTextResponse

from app.exceptions.api_exception import AppLogicException


class JsonAPIErrorsMiddleware(BaseHTTPMiddleware):
    _logger = logging.getLogger(__name__)

    async def dispatch(
        self,
        request: Request,
        call_next: RequestResponseEndpoint,
    ) -> Response:
        try:
            response = await call_next(request)

            return response
        except AppLogicException as e:
            self._logger.warning(
                f"Handled {type(e)} exception by JsonAPIErrorsMiddleware",
                exc_info=e,
                extra={"response_code": e.http_status_code, "downstream_message": e.msg},
            )
            http_status_code = e.http_status_code or status.HTTP_400_BAD_REQUEST
            return JSONResponse(
                status_code=http_status_code,
                content={
                    "errors": [
                        JsonAPIErrorSchema(
                            code=e.error_code or http_status_code,
                            source={
                                "type": f"{type(e).__module__}.{type(e).__qualname__}"
                            },
                            title=str(e.msg),
                        ).dict()
                    ],
                },
            )